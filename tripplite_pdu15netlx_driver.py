import sys
import time
import socket
import re

from typing import List, Optional

from paramiko.client import SSHClient
from paramiko import Channel

from pdu_driver_protocol import OutletState


PORT=23
NUM_OUTLETS=2

class Driver(object):
    """
    Driver for Tripplite PDU15NETLX network-controlled power outlet.
    """
    def __init__(self, host:str):
        global NUM_OUTLETS
        self.host = host
        self.error_message = ""
        self.outlet_states = [OutletState.OFF] * NUM_OUTLETS
        self.chan: Optional[Channel] = None

    # PDU protocol interface

    @property
    def outlet_count(self) -> int:
        global NUM_OUTLETS
        return NUM_OUTLETS

    def outlet_state(self, outlet_num: int) -> OutletState:
        return self.outlet_states[outlet_num-1]

    def set_outlet_state(self, outlet_num: int, desired_state: OutletState) -> None:
        try:
            chan = self.connect()
            chan.send(b"device\n")
            read_until(chan, r"device.*>")
            load_cmd = f"load {outlet_num}\n".encode("ascii")
            chan.send(load_cmd)
            read_until(chan, r"load.*>")
            state_word = desired_state.name.lower()
            state_cmd = f"{state_word}\n".encode("ascii")
            chan.send(state_cmd)
            read_until(chan, r"proceed:")
            chan.send(b"yes\n")
            chan.send(b"exit\n")
            read_until(chan, r"device.*>")
            chan.send(b"exit\n")
            read_until(chan, r"localadmin>")
        except Exception as e:
            self.error_message = f"while scanning outlets: {str(e)}"
            raise e

    def scan_outlets(self) -> None:
        try:
            chan = self.connect()
            chan.send(b"device\n")
            read_until(chan, r"device.*>")
            chan.send(b"show load\n")
            response = read_until(chan, r"device.*>")
            chan.send(b"exit\n")
            read_until(chan, r"localadmin>")
            table = read_table(response)
            state_index = table.header.index("STATE")
            self.outlet_states = [OutletState[r[state_index].upper()] for r in table.rows]
        except Exception as e:
            self.error_message = f"while scanning outlets: {str(e)}"
            raise e

    @property
    def last_error(self) -> str:
        return self.error_message

    # device specific interface
    def connect(self) -> Channel:
        if self.chan is None or self.chan.closed:
            client = SSHClient()
            client.load_system_host_keys()
            client.connect(self.host, username="localadmin", password="Admin123")
            self.chan = client.invoke_shell()
            self.chan.settimeout(1)
            read_until(self.chan, r"localadmin>")
        return self.chan


def read_until(chan: Channel, pattern: str, timeout_s=60 ) -> str:
    pattern_re = re.compile(pattern)
    buffer = ""
    timeouts = 0
    while not pattern_re.search(buffer) and timeouts < timeout_s:
        try:
            new_bytes = chan.recv(1000)
            sys.stdout.write(new_bytes.decode("ascii"))
            sys.stdout.flush()
            buffer += new_bytes.decode("ascii")
        except socket.timeout:
            timeouts += 1
    if not pattern_re.search(buffer):
        raise socket.timeout
    return buffer


class Table(object):
    def __init__(self, header: List[str], rows: List[List[str]]):
        self.header = header
        self.rows = rows

    def __str__(self) -> str:
        out = [" ".join(self.header)]
        for line in self.rows:
            out.append( " ".join(line))
        return "\n".join(out)


def get_starts(dashline: str) -> List[int]:
    """
    return column starts for dash table column separators
    """
    widths = [len(s) for s in dashline.split()]
    starts = [0]
    for i, w in enumerate(widths):
        starts.append(starts[i] + w + 2)
    return starts


def split_on_columns(line: str, starts: List[int]) -> List[str]:
    """
    Split a line based on column starts returned from get_starts
    """

    return [line[starts[i]:starts[i+1]-2].strip() for i in range(len(starts)-1)]


def read_table(buffer: str) -> Table:
    state = "looking for dashes"
    lines = buffer.split("\n")
    last_line = ""
    header = []
    rows = []
    for line in lines:
        if state == "looking for dashes":
            if line[0] == "-":
                state = "reading table"
                starts = get_starts(line)
                header = split_on_columns(last_line, starts)
            else:
                last_line = line
        elif state == "reading table":
            if len(line.split()) < 2:
                state = "end of table"
            else:
                rows.append(split_on_columns(line, starts))
    return Table(header, rows)

if __name__ == "__main__":
    d = Driver("pwr-exvea-pem0")
    # chan = d.connect()
    print(d.outlet_states)
    d.scan_outlets()
    d.set_outlet_state(1, OutletState.OFF)
    d.scan_outlets()
    print(d.outlet_states)

