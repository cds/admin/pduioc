#!/usr/bin/env python3
"""Python EPICS IOC to control Pulizzi power control unit.

An IOC which passively waits until a user command is received,
it then handles that command and returns to the passive state.

Commands are:
    Turn individual outlet ON
    Turn all outlets ON
    Turn individual outlet OFF
    Turn all outlets OFF
    Rescan the outlet states

D.Barker LHO July 2018 First py2.7 version written as part of SURF project

D.Barker LHO Apr2023 Upgrade to py3, PEP8
"""
import telnetlib
import time
import datetime
import argparse

from pdu_driver_protocol import OutletState

PORT = 23
DELAY_TIME = 0.2




class Driver(object):
    def __init__(self, host):
        self.host = host
        self.err_msg = ""
        self.outlet_states = [OutletState.OFF] * self.outlet_count

    @property
    def outlet_count(self) -> int:
        return 4

    def scan_outlets(self) -> None:
        try:
            tn = telnetlib.Telnet(self.host, PORT)
        except Exception as e:
            self.err_msg = "Error: telnet to host {} failed".format(self.host)
            raise e

        LOGIN_RESPONSE = b"IPC ONLINE!"

        tn.write(b"@@@@\r\n")
        response = tn.read_until(LOGIN_RESPONSE, 5).decode('ascii')
        print(response)
        time.sleep(DELAY_TIME)

        tn.write(b"DN0\r\n")
        response = tn.read_until(b"OUTLET-4", 5).decode('ascii')
        print(response)
        time.sleep(DELAY_TIME)

        for index in range(1, 5):
            if "OUTLET {} ON".format(index) in response:
                print("outlet {} is ON".format(index))
                self.outlet_states[index-1] = OutletState.ON
            else:
                print("outlet {} is OFF".format(index))
                self.outlet_states[index-1] = OutletState.OFF
        # tn.write(b"LO\r\n")

    def set_outlet_state(self, outlet: int, desired_state: OutletState) -> None:
        try:
            tn = telnetlib.Telnet(self.host, PORT)
        except Exception as e:
            self.err_msg = "Error: telnet to host {} failed".format(self.host)
            print(self.err_msg)
            raise e

        LOGIN_RESPONSE = b"IPC ONLINE!"

        tn.write(b"@@@@\r\n")
        response = tn.read_until(LOGIN_RESPONSE, 5).decode('ascii')
        print(response)
        time.sleep(DELAY_TIME)

        if desired_state == OutletState.ON:
            cmd = "N0{}\r\n".format(outlet)
        elif desired_state == OutletState.OFF:
            cmd = "F0{}\r\n".format(outlet)
        tn.write(cmd.encode('utf-8'))
        response = tn.read_until(b"J4 )", 5).decode('ascii')
        print(response)
        time.sleep(DELAY_TIME)
        # tn.write(b"LO\r\n")

        # update outlet status from response
        for index in range(1, 5):
            if "OUTLET {} ON".format(index) in response:
                print("outlet {} is ON".format(index))
                self.outlet_states[index-1] = OutletState.ON
            else:
                print("outlet {} is OFF".format(index))
                self.outlet_states[index-1] = OutletState.OFF


    def outlet_state(self, outlet: int) -> OutletState:
        return self.outlet_states[outlet - 1]

    @property
    def last_error(self) -> str:
        return self.err_msg