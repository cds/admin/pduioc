from typing import Protocol
from enum import Enum

class OutletState(Enum):
    ON = 1
    OFF = 2

class PDUDriver(Protocol):
    """
    A duck-typing interface for static type checks of power supply drivers
    """
    def __init__(self, host: str):
        pass

    @property
    def outlet_count(self) -> int:
        return 0

    def outlet_state(self, outlet_num: int) -> OutletState:
        return OutletState.OFF

    def set_outlet_state(self, outlet_num: int, desired_state: OutletState) -> None:
        pass

    def scan_outlets(self) -> None:
        pass

    @property
    def last_error(self) -> str:
        return ""