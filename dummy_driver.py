from pdu_driver_protocol import OutletState

class Driver(object):
    """
    Dummy PDU driver useful for testing
    """
    def __init__(self, host):
        self.host = host

    @property
    def outlet_count(self) -> int:
        return 4

    def outlet_state(self, outlet_num: int) -> OutletState:
        return OutletState.OFF

    def set_outlet_state(self, outlet_num: int, desired_state: OutletState) -> None:
        print(f"DUMMY: turn outlet {outlet_num} {desired_state.name}")

    def scan_outlets(self) -> None:
        print(f"DUMMY: scan outlets")

    @property
    def last_error(self) -> str:
        return ""
