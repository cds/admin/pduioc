#!/usr/bin/env python3
"""Python EPICS IOC to control remote power units.

An IOC which passively waits until a user command is received,
it then handles that command and returns to the passive state.

It's based off of a Pulizzi specific IOC written by Dave Barker in
h1_pulizzi_power_control_ioc.py


Commands are:
    Turn individual outlet ON
    Turn all outlets ON
    Turn individual outlet OFF
    Turn all outlets OFF
    Rescan the outlet states

Erik von Reis 2023
"""

import time
from datetime import datetime
import argparse
import platform
import re
from importlib import import_module
from typing import Any

from pdu_driver_protocol import PDUDriver, OutletState

# get the hardware-specific drivers

PDU_DRIVERS = {
    "PDU15NETLX": "tripplite_pdu15netlx_driver",
    "PULIZZI": "pulizzi_driver",
    "DUMMY": "dummy_driver",
}

from pcaspy import Driver, SimpleServer

OUTLET_SWITCH_COMMAND_RE = re.compile(r"OUTLET_(\d+)_([^_]+)_CMD")

OUTLET_STATES = "ON OFF".split()


def get_timestamp() -> str:
    """Return current time as string

    """
    now = datetime.now()
    timestamp = now.strftime("%a %d %b %H:%M:%S %Y")
    return timestamp


class PDUIOC(Driver):
    def __init__(self, pdu_driver: PDUDriver):
        Driver.__init__(self)
        self.pdu_driver = pdu_driver
        self.setParam("IOC_HOST", str(platform.node()))
        self.setParam("IOC_PROCESS", "systemd")
        timestring = datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        self.setParam("IOC_START_TIME", timestring)
        self.scan_outlets()

    def write(self, reason: str, value: Any) -> None:
        # take proper actions
        global OUTLET_SWITCH_COMMAND_RE, OUTLET_STATES
        if reason == 'OUTLETS_SCAN_CMD':
            print("scanning outlets status")
            self.scan_outlets(report=True)
        m = OUTLET_SWITCH_COMMAND_RE.match(reason)
        if m:
            try:
                outlet_number = int(m.group(1))
            except ValueError:
                print(f"Bad outlet number in command {reason}. Could not be turned into an integer.")
                return
            if outlet_number < 1:
                print(f"outlet number must be a positive number in {reason}.  Was {outlet_number}")
                return
            if outlet_number > self.pdu_driver.outlet_count:
                print(f"outlet number {outlet_number} > {self.pdu_driver.outlet_count}, taken from {reason}")
                return
            desired_state = m.group(2)
            if desired_state not in OUTLET_STATES:
                print(f"Command {reason} requested an unknown outlet state {desired_state}")
                return
            print(f"turning outlet {outlet_number} {desired_state}")
            self.set_outlet_state(outlet_number, OutletState[desired_state])
            time.sleep(1.0)
            self.scan_outlets()
        if reason == 'OUTLETS_ALL_OFF_CMD':
            print("turning all outlets OFF")
            self.turn_all_outlets_off()
            time.sleep(1.0)
            self.scan_outlets()

    def scan_outlets(self, report: bool = False) -> None:
        self.setParam('UNIT_STATUS', 1)
        self.updatePVs()
        try:
            self.pdu_driver.scan_outlets()
        except Exception as e:
            print(f"error scanning outlets: {e}")
            self.setParam("UNIT_ERR_MESSAGE", self.pdu_driver.last_error)
            self.updatePVs()
            self.setParam('UNIT_STATUS', 0)
            return
        for outlet_num in range(1, self.pdu_driver.outlet_count + 1):
            current_state = self.pdu_driver.outlet_state(outlet_num)
            chan = f"OUTLET_{outlet_num}_STATUS"
            self.setParam(chan, current_state.value)
            print(f"outlet {outlet_num} is {current_state.name}")
        time.sleep(1.0)
        if report:
            timestring = datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
            message_string = timestring + "SCAN"
            self.setParam('UNIT_MESSAGE', message_string)
        self.setParam("UNIT_ERR_MESSAGE", "")
        self.setParam('UNIT_STATUS', 0)
        self.updatePVs()

    def set_outlet_state(self, outlet_num: int, desired_state: OutletState) -> None:
        self.setParam('UNIT_STATUS', 1)
        self.updatePVs()
        try:
            self.pdu_driver.set_outlet_state(outlet_num, desired_state)
        except Exception as e:
            print(f"error setting outlet {outlet_num} to state {desired_state.name}: {e}")
            self.setParam("UNIT_ERR_MESSAGE", self.pdu_driver.last_error)
            self.setParam('UNIT_STATUS', 0)
            self.updatePVs()
            return
        current_state = self.pdu_driver.outlet_state(outlet_num)
        chan = f"OUTLET_{outlet_num}_STATUS"
        self.setParam(chan, current_state.value)
        print(f"outlet {outlet_num} is {current_state.name}")
        timestring = datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        message_string = timestring + "OUTLET {} {}".format(outlet_num, desired_state.name )
        self.setParam('UNIT_MESSAGE', message_string)
        self.setParam("UNIT_ERR_MESSAGE", "")
        self.setParam('UNIT_STATUS', 0)
        self.updatePVs()

    def turn_all_outlets_off(self) -> None:
        error = False
        self.setParam('UNIT_STATUS', 1)
        self.updatePVs()
        for outlet_num in range(1, self.pdu_driver.outlet_count + 1):
            try:
                self.pdu_driver.set_outlet_state(outlet_num, OutletState.OFF)
            except Exception as e:
                error = True
                print(f"error turning off all outlets: {e}")
                self.setParam("UNIT_ERR_MESSAGE", self.pdu_driver.last_error)
        timestring = datetime.now().strftime("%Y-%m-%d %H:%M:%S ")
        message_string = timestring + "ALL OFF"
        self.setParam('UNIT_MESSAGE', message_string)
        if not error:
            self.setParam("UNIT_ERR_MESSAGE", "")
        self.setParam('UNIT_STATUS', 0)
        self.updatePVs()


import pulizzi_driver

def get_pdu_driver(host: str, pdu_part_num: str) -> PDUDriver:
    """
    Construct a pdu driver object.
    """
    global PDU_DRIVERS
    if pdu_part_num in PDU_DRIVERS:
        mod = import_module(PDU_DRIVERS[pdu_part_num])
        return mod.Driver(host)
    else:
        raise Exception(f"Unknown power supply part number{pdu_part_num}")


def create_pvs(server: SimpleServer, prefix: str, pdu_driver: PDUDriver) -> None:
    pvdb = {
        'OUTLETS_SCAN_CMD': {
            'type': 'int',
            'asyn': True
        },
        'OUTLETS_ALL_OFF_CMD': {
            'type': 'int',
            'asyn': True
        },
        'UNIT_STATUS': {
            'type': 'enum',
            'enums': ['DONE', 'BUSY']
        },
        'UNIT_ERROR': {
            'type': 'string',
        },
        'UNIT_MESSAGE': {
            'type': 'string',
        },
        'UNIT_ERR_MESSAGE': {
            'type': 'string',
        },
        'IOC_HOST': {
            'type': 'string',
        },
        'IOC_PROCESS': {
            'type': 'string',
        },
        'IOC_START_TIME': {
            'type': 'string',
        },
    }
    for outlet_num in range(1, pdu_driver.outlet_count + 1):
        pvdb[f"OUTLET_{outlet_num}_ON_CMD"] = {
            'type': 'int',
            'asyn': True
        }
        pvdb[f"OUTLET_{outlet_num}_OFF_CMD"] = {
            'type': 'int',
            'asyn': True
        }
        pvdb[f"OUTLET_{outlet_num}_STATUS"] = {
            'type': 'int',
            'asyn': True
        }
    server.createPV(prefix, pvdb)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('host')
    parser.add_argument('prefix')
    parser.add_argument('pdupart', choices=PDU_DRIVERS.keys())

    args = parser.parse_args()

    g_pdu_driver = get_pdu_driver(args.host, args.pdupart)

    g_server = SimpleServer()
    create_pvs(g_server, args.prefix, g_pdu_driver)
    driver = PDUIOC(g_pdu_driver)

    while True:
        # process CA transactions
        g_server.process(0.1)